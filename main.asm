extern string_length
extern print_string
extern print_string_err
extern print_newline
extern print_newline_err
extern string_equals
extern read_word
extern exit

extern find_word

global _start

section .rodata
wrong_key: db "Key read error", 0
not_found: db "This string wasn't found in list", 0

section .bss
buffer: resb 256

section .text
%include "colon.inc"
%include "words.inc"


_start:
    mov rsi, 255
    mov rdi, buffer
    call read_word
    test rax, rax
    je wrong_key_error
    mov rdi, buffer
    mov rsi, list
    call find_word
    test rax, rax
    je not_found_error
    jmp success
wrong_key_error:
    mov rdi, wrong_key
    call print_string_err
    call print_newline_err
    mov rdi, 1
    call exit
not_found_error:
    mov rdi, not_found
    call print_string_err
    call print_newline_err
    mov rdi, 2
    call exit
success:
    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    mov rdi, 0
    call exit
